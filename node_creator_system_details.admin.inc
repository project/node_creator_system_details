<?php

/**
 * @file
 * Admin configure inc file.
 */

/**
 * Implement for Admin setting page for node_creator_system_details.
 */
function node_creator_system_details_admin_settings($form, &$form_state) {
  $node_types = node_type_get_names();
  $form['node_creator_system_details_settings'] = array(
    '#type' => 'checkboxes',
    '#options' => $node_types,
    '#title' => t('Select content type'),
    '#description' => t("Select content type which you want to save creator system detais."),
    '#default_value' => variable_get('node_creator_system_details_settings', array('select')),
  );
  return system_settings_form($form);
}
