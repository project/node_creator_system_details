
*******************************************************
    README.txt for Node Creator System Details module
*******************************************************

Contents:
=========
1. ABOUT
2. REQUIREMENTS
3. USAGES
4. INSTALLATION

1. ABOUT
===========
Node Creator System Details By module saves node author system details. This is 
very helpful in sites where particular node author details report.

2. REQUIREMENTS
================

* Views module.

3. USAGES
================

* In user views you can add the relationship to Node creator system details:
    * Browser
    * IP
    * Operating System
* after adding the relationship you can add as many fields you want 
  and set relationship to Creator Nid field.

3. INSTALLATION
================

* Install as usual, 
  see https://drupal.org/documentation/install/modules-themes/modules-7 for 
  further information.
